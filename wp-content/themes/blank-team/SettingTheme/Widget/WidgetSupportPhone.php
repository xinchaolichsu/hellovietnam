<?php
/**
 * WidgetSupportPhone
 * @author: Garung <daudq.info@gmail.com>
 */
namespace App\Widget;

use MSC\Widget;


class WidgetSupportPhone extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => 'phone_support',
            'label'       => __('Support Phone', THEME_NAME),
            'description' => 'This widget shows product by category',
        ];

        $fields = [
            [
                'label' => __('Chi nhánh Hà Nội', THEME_NAME),
                'name'  => 'title_hanoi',
                'type'  => 'text',
            ],
            [
                'label' => __('Số điện thoại', THEME_NAME),
                'name'  => 'phone_hanoi',
                'type'  => 'text',
            ],
            [
                'label' => __('Chi nhánh Sài Gòn', THEME_NAME),
                'name'  => 'title_saigon',
                'type'  => 'text',
            ],
            [
                'label' => __('Số điện thoại', THEME_NAME),
                'name'  => 'phone_saigon',
                'type'  => 'text',
            ],
        ];

        parent::__construct($widget, $fields);
    }

    /**
     * [handle description]
     * @param  [type] $instance [description]
     * @return [type]           [description]
     */
    public function handle($instance)
    {
        ?>
		<div class="hotline">
            <table>
			    <tbody>
			        <tr>
			            <td>
			            	<?php echo $instance['title_hanoi'] ?>
			            </td>
			            <td>
			            	<a href="tel:<?php echo $instance['phone_hanoi'] ?>"><?php echo $instance['phone_hanoi'] ?></a>
			            </td>
			        </tr>
			        <tr>
			            <td>
			            	<?php echo $instance['title_saigon'] ?>
			            </td>
			            <td>
			            	<a href="tel:<?php echo $instance['phone_saigon'] ?>"><?php echo $instance['phone_saigon'] ?></a>
			            </td>
			        </tr>
			    </tbody>
			</table>
		</div>
		<?php
    }
}