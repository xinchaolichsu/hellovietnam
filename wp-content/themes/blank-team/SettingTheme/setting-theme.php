<?php 
/**
 * SettingHellovietnam
 * @version: 1.0.0
 * @author: Garung <daudq.info@gmail.com>
 */
namespace App;

use App\Widget\WidgetSupportPhone;

class SettingHellovietnam {
    function __construct() {
        $this->init();
        $this->create_menu();
        add_action('custom_paginate', [$this, 'custom_paginate'], 10, 2);
        add_action('widgets_init', [$this, 'widget_for_contact']);
        add_action('widgets_init', [$this, 'widgets_for_sidebar']);

        add_action('widgets_init', [$this, 'widget_for_theme']);
        add_action( 'pre_get_posts', [$this, 'wpse74325_pre_get_posts'] );
        
    }

    public function init() {
        add_theme_support('post-thumbnails');
        add_image_size( 'news-thumb-home', 262, 152, true);
        add_theme_support('custom-logo', array([
            'width' => 160
        ]));
    }

    public function create_menu() {
        register_nav_menus([
            'main_nav' => 'Main Navigation_Menu'
        ]);
    }

    public function custom_paginate($paged, $total_pages) {
        if ($total_pages <= 1) {
            return;
        }
        if ($total_pages > 1) {
            $current_page = max(1, $paged);

            echo paginate_links([
                'base' => get_pagenum_link(1) . '%_%',
                'format' => '/?paged=%#%',
                'current' => $paged,
                'total' => $total_pages,
            ]);
        }
    }

    public function widget_for_theme() {
        register_sidebar([
            'name' => __('Header', THEME_NAME),
            'id' => 'header_sidebar',
            'description' => __('Hiển thị nội dung trên header', THEME_NAME),
            'before_widget' => '<aside id="%1$s" class="widget %2$s pull-left">',
            'after_widget' => '</aside>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ]);
        register_sidebar([
            'name' => __('Chân trang 1', THEME_NAME),
            'id' => 'footer-col-1',
            'description' => __('Hiển thị nội dung cho chân trang cột 1', THEME_NAME),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ]);
        register_sidebar([
            'name' => __('Chân trang 2', THEME_NAME),
            'id' => 'footer-col-2',
            'description' => __('Hiển thị nội dung cho chân trang cột 2', THEME_NAME),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ]);
        register_sidebar([
            'name' => __('Chân trang 3', THEME_NAME),
            'id' => 'footer-col-3',
            'description' => __('Hiển thị nội dung cho chân trang cột 3', THEME_NAME),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ]);
        register_sidebar([
            'name' => __('Chân trang 4', THEME_NAME),
            'id' => 'footer-col-4',
            'description' => __('Hiển thị nội dung cho chân trang cột 4', THEME_NAME),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ]);
    }
    public function widget_for_contact() {
        register_sidebar([
            'name' => __('Tin Tức - Sự Kiện', THEME_NAME),
            'id' => 'sidebar2',
            'description' => __('Add widgets here to show form for user.', 'galaxy'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ]);

        register_sidebar([
            'name' => __('Chương Trình - Dịch Vụ', THEME_NAME),
            'id' => 'sidebar3',
            'description' => __('Add widgets here to show map.', 'galaxy'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ]);
    }

    public function widgets_for_sidebar() {
        register_sidebar([
            'name' => __('Thông Tin Viêc Lam - Đào Tạo', THEME_NAME),
            'id' => 'sidebar4',
            'description' => __('Add widgets here to show a image.', 'galaxy'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ]);
        register_sidebar([
            'name' => __('Liên Kết', THEME_NAME),
            'id' => 'sidebar5',
            'description' => __('Add widgets here to show a video.', 'galaxy'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ]);
        
    }

    public function wpse74325_pre_get_posts($query) {
        if ($query->is_main_query() || is_category()) {
            $query->set('posts_per_page', '10');
        }
    }
}