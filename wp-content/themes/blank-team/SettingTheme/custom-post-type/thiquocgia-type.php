<?php

	/* ==================================================

	Books Post Type Functions

	================================================== */
	$args = array(
	    "label" 						=> _x('Category', 'category label', THEME_NAME),
	    "singular_label" 				=> _x('Thi quốc gia', 'category singular label', THEME_NAME),
	    'public'                        => true,
	    'hierarchical'                  => true,
	    'show_ui'                       => true,
	    'show_in_nav_menus'             => true,
	    'args'                          => array( 'orderby' => 'term_order' ),
        'rewrite'                       => array(
                                            'slug' => 'thi-quoc-gia',
                                            'with_front' => false ),
	    'query_var'                     => true
	);

	register_taxonomy( 'thi-quoc-gia', 'thiquocgia', $args );


	add_action('init', 'thiquocgia_register');

	function thiquocgia_register() {

	    $labels = array(
	        'name' => _x('Thi quốc gia', 'post type general name', THEME_NAME),
	        'singular_name' => _x('Thi quốc gia', 'post type singular name', THEME_NAME),
	        'add_new' => _x('Add New', 'job', THEME_NAME),
	        'add_new_item' => __('Add New', THEME_NAME),
	        'edit_item' => __('Edit ', THEME_NAME),
	        'new_item' => __('New', THEME_NAME),
	        'view_item' => __('View', THEME_NAME),
	        'search_items' => __('Search', THEME_NAME),
	        'not_found' =>  __('Không sự kiện nào được thêm', THEME_NAME),
	        'not_found_in_trash' => __('Không có gì trong thùng rác', THEME_NAME),
	        'parent_item_colon' => ''
	    );

	    $args = array(
	        'labels' => $labels,
	        'public' => true,
	        'show_ui' => true,
	        'show_in_menu' => true,
	        'show_in_nav_menus' => true,
            'rewrite' =>  array('slug' => 'bai-viet-thi-quoc-gia','with_front' => false ),
	        'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
	        'has_archive' => true,
	        'taxonomies' => array('thi-quoc-gia', 'post_tag',)
	       );

	    register_post_type( 'thiquocgia' , $args );
	}
	add_filter("manage_edit-thiquocgia_columns", "thiquocgia_edit_columns");
	function thiquocgia_edit_columns($columns){
	        $columns = array(
	            "cb" => "<input type=\"checkbox\" />",
	            "thumbnail" => "",
	            "title" => __("Tin tức - sự kiện", THEME_NAME),
	            "description" => __("Description", THEME_NAME),
	            "thi-quoc-gia" => __("Categories", THEME_NAME)
	        );

	        return $columns;
	}

?>