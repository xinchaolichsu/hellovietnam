<div class="container slider">
	<div id="carousel-id" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#carousel-id" data-slide-to="0" class=""></li>
			<li data-target="#carousel-id" data-slide-to="1" class=""></li>
			<li data-target="#carousel-id" data-slide-to="2" class="active"></li>
		</ol>
		<div class="carousel-inner">
			<div class="item">
				<img alt="First slide" src="<?php echo bloginfo('template_directory');?>/images/slide1.jpg">
			</div>
			<div class="item">
				<img alt="Second slide" src="<?php echo bloginfo('template_directory');?>/images/banner3.png">
			</div>
			<div class="item active">
				<img alt="Third slide" src="<?php echo bloginfo('template_directory');?>/images/slide2.jpg">
			</div>
		</div>
		<a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
		<a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
	</div>
</div><!--end slider-->