<?php
/*
Template Name:su_kien
*/
get_header();
?>

<div class="content tintuc_sukien about">

	<div class="container">
		<div class="row Module Module-141">
			<ol class="breadcrumb" style="background: none;">
	            <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="#" class="itemcrumb" itemprop="url"><span itemprop="title">Trang chủ</span></a></li>
	        
	            <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="#" class="itemcrumb active" itemprop="url"><span itemprop="title">Tin tức - sự kiện</span></a></li>		    
			</ol>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div id="ctl00_divCenter" class="col-md-9 col-main">
			    <div class="new Module Module-77">
				    <div class="ModuleContent">
					    <h1 class="title-page">Tin tức - sự kiện</h1>
					    <section class="row">
							<?php $additional_loop = new WP_Query("cat=7&paged=$paged&posts_per_page=9"); ?>

							<?php while ($additional_loop->have_posts()) : $additional_loop->the_post(); ?>
								<?php $i++?>
								<?php $pages = $wp_query->max_num_pages;?>
								<article class="news-item mrb30 col-sm-4">
							    <a class="center-block text-center" href="#" target="_self">
										<?php the_post_thumbnail(); ?>
									</a>

									<h2>
										<a href="<?php the_permalink();?>" target="_self">
											<?php the_title(); ?>
										</a>
									</h2>

									<time><?php the_time('F j,Y'); ?></time>

									<p>
										<span style="font-family: Arial;">
											<?php the_excerpt(); ?>
										</span>
									</p>
						    	</article>
								<?php if( $i == 3 || $i ==6){
										echo "<div style='clear:both'></div>";
								} ?>

							<?php endwhile; wp_reset_postdata();?>
					    </section>
							    
						</div>
						<div id="ctl00_mainContent_ctl00_ctl00_divPager" class="pages newspager">
						    <div class="modulepager">
							    <ul class="pagination">
									<li">
										<span><?php kriesi_pagination($additional_loop->max_num_pages);?></span> 
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			
			<div id="ctl00_divRight" class="col-md-3 col-right cmszone">
			    <?php get_template_part('sidebar'); ?>			    
			</div>

		</div>
	</div>
</div>




<?php get_footer(); ?>