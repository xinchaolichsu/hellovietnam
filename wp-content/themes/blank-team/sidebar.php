<aside>
        <!-- All this stuff in here only shows up if you DON'T have any widgets active in this zone -->
            <?php if(is_active_sidebar('sidebar1')):?>
            <div class="Module Module-153">
                <div class="ModuleContent">
                    <nav class="right-menu topfix" style="width: 261.5px;"><h4>Về JVNet</h4>
                        <ul class="nav">
                            <li>
                                <a href="#part-1" target="_self"><i class="fa fa-angle-double-right"></i> Giới thiệu tổng quan</a>
                            </li>

                            <li>
                                <a href="#part-2" target="_self"><i class="fa fa-angle-double-right"></i> Giới thiệu</a>
                            </li>

                            <li>
                                <a href="#part-3" target="_self"><i class="fa fa-angle-double-right"></i> Tầm nhìn - Sứ mệnh</a>
                            </li>

                            <li>
                                <a href="#part-4" target="_self"><i class="fa fa-angle-double-right"></i> Giá trị cốt lõi</a>
                            </li>

                            <li>
                                <a href="#part-5" target="_self"><i class="fa fa-angle-double-right"></i> Lịch sử hình thành</a>
                            </li>
                        <?php dynamic_sidebar('sidebar1'); ?>
                        </ul>
                    </nav>
                </div>
          </div>
          <?php endif; ?>
        <?php if(is_active_sidebar('sidebar2')) :?>
        <div class="clearfix right-menu mrb20 Module Module-142">
            <div class="ModuleContent">
                <nav class="right-menu"><h4>Tin tức - sự kiện</h4>
                      <?php wp_nav_menu(array('menu' => 'Tin Tức - Sự Kiện Menu', 'container' => ''));?>


                    <?php dynamic_sidebar('sidebar2'); ?>
                      </ul>
                  </nav>
              </div>
        </div>
        <?php endif; ?>

        <?php if(is_active_sidebar('sidebar3')) :?>
        <div class="clearfix right-menu mrb20 Module Module-142">
            <div class="ModuleContent">
            <nav class="right-menu">
                <h4>Chương trình - Dịch vụ</h4>
                  <ul class="nav">
                    <li class="active">
                        <a href="#" target="_self"><i class="fa fa-angle-double-right">
                        </i> Thực tập sinh kỹ năng</a>
                    </li>

                    <li>
                        <a href="#" target="_self"><i class="fa fa-angle-double-right"></i> Chương trình kỹ sư</a>
                    </li>

                    <li>
                        <a href="#" target="_self">
                        <i class="fa fa-angle-double-right"></i> Giới thiệu việc làm</a>
                    </li>

                    <li>
                        <a href="#" target="_self">
                        <i class="fa fa-angle-double-right"></i> Du học Nhật Bản</a>
                    </li>
                <?php dynamic_sidebar('sidebar3'); ?>
                  </ul>
              </nav>
            </div>
       </div>
        <?php endif;?>

        <?php if(is_active_sidebar('sidebar4')) : ?>
           <div class="clearfix right-menu mrb20 Module Module-142">
               <div class="ModuleContent">
                   <nav class="right-menu">
                       <h4>Thông tin việc làm - đào tạo</h4>
                      <ul class="nav">
                        <li>
                            <a href="#" target="_self">
                            <i class="fa fa-angle-double-right"></i> Tại Nhật Bản</a>
                        </li>

                        <li>
                            <a href="#" target="_self">
                            <i class="fa fa-angle-double-right"></i> Tại Việt Nam</a>
                        </li>
                    <?php dynamic_sidebar('sidebar4'); ?>
                      </ul>
                  </nav>
              </div>
          </div>
      <?php endif;?>


        <?php if(is_active_sidebar('sidebar5')) :?>
        <div class="clearfix mrb20 lien-ket Module Module-143">
            <div class="ModuleContent">
                <h2 class="module-title">Liên kết</h2>
            <?php dynamic_sidebar('sidebar5'); ?>
            </div>
        </div>
        <div class="clearfix mrb30 quy-trinh Module Module-144">
            <div class="ModuleContent">
            <h2 class="module-title">Quy trình</h2>
                <table>
                    <tbody>
                        <tr>
                            <td>
                            <em class="fa fa-newspaper-o"></em>
                            </td>
                            <td>
                            <a href="#">Tìm hiểu về quy trình tuyển chọn, hồ sơ và thủ tục</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif; ?>

</aside>