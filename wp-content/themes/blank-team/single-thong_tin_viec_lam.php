<?php get_header(); ?>
<div class="content single-vieclam">
	<div class="container">
		<div class="section-breadcrum">
			<ol class="breadcrumb">
	            <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
		            <a href="#" class="itemcrumb" itemprop="url">
		            	<span itemprop="title">Trang chủ</span>
		            </a>
	            </li>
	        
	            <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
		            <a href="#" class="itemcrumb active" itemprop="url">
		            	<span itemprop="title">Thông tin việc làm - đào tạo</span>
		            </a>
	            </li>
			</ol>
		</div>


		<div class="section-content">
			<div class="row">
				<div id="ctl00_divCenter" class="col-md-9 col-main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
							
							<h2><?php the_title(); ?></h2>
							
							<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>
							<hr>
							<div class="entry">
								
								<?php the_content(); ?>

								<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
								
								<?php the_tags( 'Tags: ', ', ', ''); ?>

							</div>
							
						</div>

					<?php endwhile; endif; ?>

					<span class="textexposedshow" style="font-size: 14px;">
						-------------------------------------------------
					</span><br>

					<strong style="font-family: Arial;">
						<span class="textexposedshow" style="font-family: Arial; color: #1d2129; background: white; font-size: 14px;">◆</span>
						<span class="textexposedshow" style="font-family: Arial; color: #1d2129; background: white; font-size: 13pt;"> </span>
						<span class="textexposedshow" style="font-family: Arial; font-size: 14px;">Văn phòng JVNET tại Hà Nội</span>
					</strong>

					<span style="font-family: Arial; font-size: 13pt;"><br>

					<span class="textexposedshow" style="font-family: Arial; font-size: 14px;">- Tầng 7, tòa nhà 148 Hoàng Quốc Việt, Cầu Giấy, Hà Nội</span><br>

					<span class="textexposedshow" style="font-family: Arial;">-</span><span class="textexposedshow" style="font-family: Arial; font-size: 14px;"> Điện thoại: 04-37556251</span><br>

					<span class="textexposedshow" style="font-family: Arial;">- </span><span class="textexposedshow" style="font-size: 14px;">Di động: A Hữu: 0989-501-009 / A Quân: 0916-387-598</span><br>

					</span>

					<strong style="font-family: Arial;">
						<span class="textexposedshow" style="font-family: Arial; color: #1d2129; background: white; font-size: 14px;">◆</span>
						<span class="textexposedshow" style="font-family: Arial; color: #1d2129; background: white; font-size: 13pt;"> </span>
						<span class="textexposedshow" style="font-family: Arial; font-size: 14px;">Văn phòng JVNET tại TP HCM</span>
					</strong>

					<span style="font-family: Arial; font-size: 13pt;"><br>
						<span class="textexposedshow" style="font-family: Arial;">
							<span style="font-family: Arial;">- </span>

							<span style="font-size: 14px;">Lầu 4, tòa nhà HT Building, 132-134 D2, Bình Thạnh, HCM</span>
						</span><br>

						<span class="textexposedshow" style="font-family: Arial;">- </span>

						<span class="textexposedshow" style="font-family: Arial; font-size: 14px;">Điện thoại: 08-62948869
						</span><br>

						<span class="textexposedshow" style="font-family: Arial;">- </span>

						<span class="textexposedshow" style="font-size: 14px;">Di động: A Nghĩa: 0979-837-138/ A Tân: 0943-066-113</span>
					</span>
				</div>

				<div id="ctl00_divRight" class="col-md-3 col-right">
					<?php get_template_part('sidebar'); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>