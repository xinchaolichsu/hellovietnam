<footer id="footerSection">
	<div class="container">
		<div class="row footer-up">
			<div class="footer-menu">
				<div class="text-footer">
					<div class="col-md-3 footer-col-1 mrb15">
						<div class="col-md-12 col-sm-12 col-xs-12 mrb15 logo-footer-1">
							<a href="<?php echo get_home_url(); ?>">
								<?php the_custom_logo();?>
							</a>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<?php 
							if(is_active_sidebar('footer-col-1')) {
								dynamic_sidebar('footer-col-1');
							}
							?>							
						</div>
						<!-- <h5>Về JVNet</h5> -->
					</div>

					<div class="col-md-3 mrb15">
						<?php 
						if(is_active_sidebar('footer-col-2')) {
							dynamic_sidebar('footer-col-2');
						}
						?>
					</div>

					<div class="col-md-3 mrb15">
						<?php 
						if(is_active_sidebar('footer-col-3')) {
							dynamic_sidebar('footer-col-3');
						}
						?>
					</div>

					<div class="col-md-3 mrb15">
						<?php 
						if(is_active_sidebar('footer-col-4')) {
							dynamic_sidebar('footer-col-4');
						}
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="row copyright text-center">
			Copyright <i class="fa fa-copyright" aria-hidden="true"></i> 2017 <a href="<?php echo site_url(); ?>"><?php echo str_replace('http://', '', site_url()); ?></a> All Rights Reserved.
		</div>
	</div>
</footer>
</div>

<?php wp_footer(); ?>
	<script src="<?php echo bloginfo('template_directory');?>/dist/app.js"></script>
</body>

</html>