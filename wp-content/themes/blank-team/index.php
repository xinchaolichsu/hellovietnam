<?php get_header(); 
	
	global $post;
	include (TEMPLATEPATH . '/inc/featured.php');
					
	$args = [
		'post_type' => 'post',
	    'posts_per_page' => 4,
	    'post_status' => ['publish'],
	    'orderby'=>'ASC',
	    'category_name' => 'su-kien'
	];
	
	$custom_posts = new WP_Query($args);
?>
<div class="content" id="homepage">
	<?php
		if(!empty($custom_posts)):
	?>
	<div class="container-fluid news-event news-home" style="background: url(<?php echo bloginfo('template_directory');?>/images/tintuc-home-bg.png) no-repeat bottom center #17738b;">
		<div class="container">
			<h4><strong>TIN HOT </strong>  <span>|</span>   SỰ KIỆN</h4>

			<div class="row">
				<?php
					foreach($custom_posts->posts as $item_post):
					?>
					
					<div class="col-md-3">
						<div class="newhome">
							<a class="center-block text-center " href="<?php the_permalink($item_post->ID);?>">
								<?php echo get_the_post_thumbnail($item_post->ID, 'news-thumb-home'); ?>
							</a>

							<div class="newshome-desc">
								<div class="newshome-title"><h5><a href="<?php the_permalink($item_post->ID);?>"><?php echo $item_post->post_title; ?></a></h5></div>
								<time><?php echo get_the_time('l, d/m/Y', $item_post->ID); ?></time>

								<div class="text-newshome">
									<span><?php echo wp_trim_words( $item_post->post_content, 35, null ); ?></span>
								</div>
							</div>
						</div>
					</div>
				<?php 
					endforeach; 
					wp_reset_postdata(); 
				?>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<div class="container">
		<h3 class="title-home title-donhang">Tiện ích giáo viên</h3>
		<div class="row noi-bat">
			
				<?php
					$tin_tsinh = new WP_Query(array('post_type'=>'thong_tin_viec_lam', 'posts_per_page'=> 4 ));
				?>
				<?php 
					if($tin_tsinh->have_posts()) : 
						foreach ($tin_tsinh->posts as $key_ts => $tin_ts):
				?>
				<div class="col-md-3">
					<div class="item-highlight">
						<div class="text-donhang">
							<h3><a target="_self" href="<?php the_permalink();?>"><?php echo $tin_ts->post_title; ?></a></h3>
						</div>
						
						<p>Số Lượng:<strong><?php the_field('so_luong', $tin_ts->ID); ?></strong></p>
						<p>Nơi Làm Việc:<strong><?php the_field('noi_lam_viec', $tin_ts->ID); ?></strong></p>
						<p>Ngày cập nhật:<strong><?php the_field('ngay_tuyen', $tin_ts->ID); ?></strong></p>
					</div>
					<div style="text-align: center;">
						<a href="<?php the_permalink($tin_ts->ID);?>">
							<?php echo get_the_post_thumbnail($tin_ts->ID, 'news-thumb-home'); ?>
						</a>
					</div>
				</div>
				<?php 
						endforeach; 
					endif; 
					wp_reset_query();
				?>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="ModuleContent">
					<h3 class="module-title">VIDEO HOT</h3>
					<iframe width="262" height="152" src="https://www.youtube.com/embed/u7nOxoBwzk8" frameborder="0" allowfullscreen></iframe>
					<p>
						Bộ Giáo dục và Đào tạo công bố bộ đề thi thử nghiệm kỳ thi THPT 2017
					</p>
				</div>
			</div>

			<div class="col-md-9">
				<div class="ModuleContent">
					<h3 class="title-home module-title">Ý tưởng trong giáo dục</h3>
					<div class="job-list table-responsive">
						<table class="table table-striped text-center">
							<tbody>
								<tr>
									<th class="text-left">Tên ý tưởng</th>
									<th class="text-center">Lượt view</th>
									<th class="text-center">Mô tả</th>
									<th class="text-center">Tác giả</th>
									<th class="text-center">Ngày Đăng</th>
								</tr>
								<?php
									$viec_lam = new WP_Query(array('post_type'=>'thong_tin_viec_lam', 'posts_per_page'=> 6 ));
								?>
								<?php 
									if($viec_lam->have_posts()) : while($viec_lam->have_posts()) : $viec_lam->the_post();
								?>
								<tr>
									<td class="text-left"><a href="<?php the_permalink();?>"><?php the_title();?></a></td>
									<td><?php the_field('so_luong'); ?></td>
									<td><?php the_field('noi_lam_viec'); ?></td>
									<td><?php the_field('luong'); ?></td>
									<td><?php the_field('ngay_tuyen'); ?></td>
								</tr>
								<?php endwhile; endif; wp_reset_query();?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php 
	$arr_post_type = [
		[
			'post_type_name' => 'thiquocgia',
			'name_display' => 'Thi Quốc gia 2017'
		],
		[
			'post_type_name' => 'hocduong',
			'name_display' => 'Học đường'
		],
		[
			'post_type_name' => 'duhoc',
			'name_display' => 'Du học'
		],
		[
			'post_type_name' => 'post',
			'name_display' => 'Khác'
		]
	];
	foreach ($arr_post_type as $key_type => $val_type): 
		$args = [
			'post_type' => $val_type['post_type_name'],
		    'posts_per_page' => 14,
		    'post_status' => ['publish'],
		    'orderby'=>'ASC'
		];
		
		$get_list = new WP_Query($args);
		// echo "<pre>";
		// var_dump($thiquocgia); die;
		if(!empty($get_list)):
			
		?>
		<div class="container">
			<h3 class="title-home title-dichvu"><?php echo $val_type['name_display']; ?></h3>
			<div class="row">
			<?php 
			foreach ($get_list->posts as $key_post => $val_post):
				if($key_post < 4):
					$image_link = wp_get_attachment_url(get_post_thumbnail_id($val_post->ID));
			        if(empty($image_link)) {
			        	$image_link = "http://fakeimg.pl/255x160/";
			        }
			?>
				<div class="col-md-3 col-sm-3 col-xs-12 mrb30">
					<a href="<?php echo the_permalink($val_post->ID); ?>" target="_self">
						<div class="col-md-12 col-sm-12 col-xs-12 image-col-1" style="padding-left: 0; padding-right: 0px;">
							<?php echo get_the_post_thumbnail($val_post->ID, 'news-thumb-home'); ?>
						</div>	
						<div class="col-md-12 col-sm-12 col-xs-12 title-col-1" style="padding-left: 0; padding-right: 0px; text-align: justify; height: 90px; max-height: 90px; overflow: hidden;">
							<h3 style="margin-top: 10px; font-size: 20px;"><?php echo $val_post->post_title; ?></h3>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 datetime-col-1" style="padding-right: 0px;">
							<div class="pull-right"><?php echo get_the_date('l, F j, Y' , $val_post->ID ); ?></div>
						</div>
					</a>

					<div class="clearfix content-col-1" style="text-align: justify;">
						<span><?php echo wp_trim_words( $val_post->post_content, 30, null ); ?></span>
					</div>
					<a class="read-more" href="<?php echo the_permalink($val_post->ID); ?>" target="_self">| Xem thêm</a>
				</div>
			<?php 
				endif;
			endforeach; 
			?>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<?php
					foreach ($get_list->posts as $key_post => $val_post):
						if($key_post >= 4 && $key_post < 10):
					?>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
						<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp <a href="<?php echo the_permalink($val_post->ID); ?>"><?php echo $val_post->post_title; ?></a>
						</div>
					</div>
					<?php
						endif;
					endforeach; 
					?>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<?php
					foreach ($get_list->posts as $key_post => $val_post):
						if($key_post > 10 && $key_post <= 14):
					?>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
						<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp <a href="<?php echo the_permalink($val_post->ID); ?>"><?php echo $val_post->post_title; ?></a>
						</div>
					</div>
					<?php
						endif;
					endforeach; 
					?>
				</div>
			</div>
		</div>
	<?php 
		endif; 
	endforeach;
	?>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-12 quy-trinh hidden-xs" style="background: #d55449;color: #ffffff;padding: 20px;">
				<div class="tim-hieu-container">
					<div class="text-center">
						<h3><i class="fa fa-quote-left" aria-hidden="true"></i> Nhanh chóng - Chính xác - Kịp thời - Khách quan <i class="fa fa-quote-right" aria-hidden="true"></i></h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!--end content-->
<script type="text/javascript">
	jQuery(document).ready(function($){
		$('.attachment-news-thumb-home').attr({'width' : '262', 'height': '152'});
	});
</script>

<?php get_footer(); ?>