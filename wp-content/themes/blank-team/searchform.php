<form role="search" method="get" id="searchform" class="searchform msc-form" action="<?php echo esc_url(home_url('/')); ?>">
	<div>
		<input type="hidden" name="post_type" value="product" />
		<input type="text" value="" name="s" id="s" placeholder="Nhập từ khóa....">
		<button type="submit" id="searchsubmit"><i class="fa fa-search" aria-hidden="true"></i></button>
	</div>
</form>