<?php
/*
Template Name: About
*/
get_header();
?>

<div class="content about">
	<div class="container">
		<div class="row Module Module-141">
			<ol class="breadcrumb" style="background: none;">

	            <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="#" class="itemcrumb" itemprop="url"><span itemprop="title">Trang chủ</span></a></li>
	        
	            <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="#" class="itemcrumb active" itemprop="url"><span itemprop="title">Về JVNet</span></a></li>
			        
			    
			</ol>
		</div>
	</div>
	
	<div class="body-content">
		<div class="container">
			<div class="row">
				<div id="ctl00_divCenter" class="col-md-9 col-main">

			   		<div class="Module Module-88">

				   		<div class="ModuleContent">
							<div id="ctl00_mainContent_ctl00_ctl00_pnlInnerWrap">
									   
								<div class="about-us clearfix">
									<h1 class="title-page">Về JVnet</h1>
									<div class="content mrb40" id="part-1">
									    <h2>Giới thiệu tổng quan</h2>

										<div class="desc clearfix">
										<div class="row">
											<div class="col-sm-6 mrb20">
												<p style="text-align: justify;">
													<strong>
														•  Tên công ty:
													</strong>
													 Công Ty Cổ Phần Thương Mại Phát Triển Kỹ Thuật và Nhân Lực Quốc Tế.
												</p>

												<p style="text-align: justify;">
													<strong>
														•  Tên giao dịch :
													</strong> 
													International Human Resources Trading and Technology Development Joint Stock Company.
												</p>

												<p style="text-align: justify;">
													<strong>
														•  Tên viết tắt:
													</strong>
													 JVNET.,JSC  
												</p>

												<p style="text-align: justify;">
													<strong>•  Ngày Thành lập:</strong> 09/09/2005
												</p>

												<p style="text-align: justify;">
													<strong>
														•  Trụ sở chính:
													</strong> Số 30 đường Trần Cung,  Bắc Từ Liêm, Thành phố Hà Nội
												</p>

												<p style="text-align: justify;">
													<strong>
														•  Văn phòng tư vấn:
													</strong> Tầng 7 – Tòa nhà PVOil – số 148 Hoàng Quốc Việt, Cầu Giấy – Hà Nội.
												</p>
												<p style="text-align: justify;">
													<strong>
														•  Điện thoại:
													</strong> 0437556251
												</p>
											</div>

											<div class="col-sm-6 mrb20 bd">
												<p style="text-align: justify;">
													<strong>
														•  Văn phòng đại diện tại TP HCM:
													</strong> Tầng 4 , Số 132-134 , đường D2 , Phường 25 , Quận Bình Thạnh , Thành phố HCM  
												</p>

												<p style="text-align: justify;">
													<strong>
														•  Điện thoại:
													</strong> 08 22437928
												</p>

												<p style="text-align: justify;">
													<strong>
														•  Email:
													</strong> info@jvnet.com.vn 
												</p>

												<p style="text-align: justify;">
													<strong>
														•  Vốn pháp định:
													</strong> 5.000.000.000 VNĐ
												</p>

												<p style="text-align: justify;">
													<strong>
													     •   Giấy phép hoạt động dịch vụ đưa Người lao động Việt Nam đi làm việc ở nước ngoài : 
													</strong>
													số 160/BLĐTBXH-GP do Bộ Lao động Thương binh và Xã hội cấp ngày 03/10/2008.
												</p>

												<p style="text-align: justify;">
													<strong>
														•  Nhân viên:
													</strong> 150 người (Bao gồm 10 nhân viên người Nhật)
												</p>
											</div>
										</div>


										<div style="text-align: justify;"><br>
										</div></div>
									</div>


									<div class="content mrb40" id="part-2">

										<h2>Giới thiệu</h2>

										<div class="desc clearfix">
											<div class="row">
												<div class="col-sm-6 mrb40 text-right">
													<p style="text-align: justify;">
													Công Ty Cổ Phần Thương Mại Phát Triển Kỹ Thuật và Nhân Lực Quốc Tế (tên viết tắt JVNET) là doanh nghiệp ngoài quốc doanh được thành lập từng tháng 9 năm 2005. 
													</p>

													<div style="text-align: justify;">
														<span style="line-height: 1.42857;">
														Trải qua 10 năm hình thành phát triển, JVNET tự hào là doanh nghiệp hàng đầu Việt Nam trong lĩnh vực đào tạo và phái cử kỹ sư, thực tập sinh sang Nhật Bản. Mỗi năm JVNET phái cử trên 1000 người đi làm việc, tu nghiệp tại Nhật Bản. Hiện nay, JVNET đang quản lý trên 3000 kỹ sư, thực tập sinh đang làm việc, tu nghiệp.&nbsp;
														</span>
													</div>

													<p style="text-align: justify;">
													Trong những năm qua, JVNET luôn cung ứng nguồn lao động có chất lượng cho các đối tác và chủ sử dụng lao động đồng thời cam kết đảm bảo mức lương, thu nhập, các quyền và lợi ích hợp pháp cho người lao động làm việc tại Nhật Bản.
													</p>

													<p style="text-align: justify;">
													JVNET liên tục tổ chức tuyển chọn, đào tạo ngoại ngữ, chuyên môn, tay nghề và kiến thức văn hoá cần thiết cho mọi đối tượng từ lao động phổ thông đến lao động kỹ thuật và chuyên gia đáp ứng yêu cầu về nhân lực của các doanh nghiệp cũng như các tập đoàn lớn tại Nhật Bản.
													</p>

													<p style="text-align: justify;">
													Song song với các dịch vụ, JVNET còn có cơ chế hỗ trợ người lao động trong quá trình đào tạo nghề, đào tạo ngoại ngữ và giáo dục định hướng, với mức phí dịch vụ thấp nhất theo quy định của Bộ Lao động – Thương binh và Xã hội. Người lao động luôn được quan tâm, tạo điều kiện và quản lý tốt nhất từ quá trình tuyển dụng cho đến khi làm việc ở nước ngoài. 
													</p>
												</div>
												<div class="col-sm-6 text-center mrb40">
													<img alt="" src="<?php echo bloginfo('template_directory');?>/images/hinh1.jpg">
												</div>
											</div>

											<div class="row">
											
												<div class="col-sm-6 col-sm-push-6 mrb20">
													<p style="text-align: justify;">
													Chúng tôi luôn có các đầu mối liên lạc, kịp thời giúp đỡ và sẵn sàng giải quyết các vấn đề vướng mắc của Du học sinh, tu nghiệp sinh và người lao động trong thời gian học tập và thực hiện hợp đồng lao động ở Nhật Bản. JVNET luôn mong muốn được hợp tác với các bạn hàng; các đối tác; các chủ đầu tư; các tổ chức; các nghiệp đoàn của các nước quan tâm mong muốn được giới thiệu việc làm, đi tu nghiệp, xuất khẩu lao động để cùng phát triển.
													</p>

													<p style="text-align: justify;">
													Lấy sự phục vụ tận tâm làm nền tảng hoạt động, JVNET luôn khai thác những đơn hàng đảm bảo nhất với chi phí hợp lý nhất và cung cấp những lao động ưu tú nhất với thời gian nhanh nhất để tạo nên thương hiệu lớn nhất tại Việt Nam. JVNET luôn nỗ lực phấn đấu để trở thành cầu nối đáp ứng mọi nhu cầu về việc làm cho người lao động! 
													</p>
												</div>

												<div class="col-sm-6 col-sm-pull-6 mrb20 text-center" style="text-align: justify;">
													<img alt="" src="<?php echo bloginfo('template_directory');?>/images/hinh2.jpg">
												</div>
											</div>

											<div style="text-align: justify;"><br>
											</div>

										</div>

									</div>
									<div class="content mrb40" id="part-3">
										<h2>Tầm Nhìn - Sứ Mệnh</h2>
										<div class="desc clearfix">
											<div class="row">
												<div class="col-sm-6 mrb20">
													<div class="text-center">
														<p><em class="fa fa-location-arrow"></em></p>
														<h4>Tầm Nhìn</h4>
													</div>

													<p style="text-align: justify;">
														•  Xây dựng JVNET trở thành công ty cung ứng nguồn lao động hàng đầu tại Việt Nam. 
													</p>

													<p style="text-align: justify;">
														•  Cung cấp nhân lực có chất lượng cho đối tác Nhật Bản
													</p>

													<p style="text-align: justify;">
														•  Định hướng cho thế hệ trẻ khởi nghiệp vững vàng, đáp ứng hoàn toàn về nguồn nhân lực có trình độ cho xã hội và hội nhập với Quốc tế.
													</p>

													<p style="text-align: justify;">
														•  Luôn hướng đến chinh phục mục tiêu phát triển bền vững và là sự lựa chọn hàng đầu cho các đối tác và người lao động.
													</p>
												</div>


												<div class="col-sm-6 mrb20 bd">
													<div class="text-center">
														<p><em class="fa fa-users"></em></p>
														<h4>Sứ Mệnh</h4>
													</div>

													<p style="text-align: justify;">
														•  Nỗ lực phấn đấu để xây dựng niềm tin bền vững với các đối tác và người lao động.
													</p>

													<p style="text-align: justify;">
														•  Mang lại những giá trị và sự hài lòng cho người lao động bằng  những đơn hàng có chất lượng tốt nhất với chi phí hợp lý.
													</p>

													<p style="text-align: justify;">
														•  Luôn đồng hành để bảo vệ quyền và lợi ích chính đáng của người lao động tại Nhật Bản.
													</p>

													<p style="text-align: justify;">
														•  Tạo dựng một môi trường làm việc chuyên nghiệp, năng động và sáng tạo mà ở đó mỗi thành viên làm việc tận tụy hết mình, nơi hội tụ và phát triển nhân tài.
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="content mrb40" id="part-4">
									    <h2>Giá Trị Cốt Lõi</h2>
									    <div class="desc clearfix">
										    <div class="row">
												<div class="col-sm-8 mrb20">
													<p style="text-align: justify;">
														<strong>1.</strong> Người lao động là tài sản có giá trị và là nguồn sức mạnh của JVNET. 
													</p>

													<p style="text-align: justify;">
														<strong>2.</strong> Đoàn kết chính là chất keo gắn kết, tập hợp các nguồn lực về con người và vật chất cùng hướng tới mục tiêu xây dựng JVNET trở thành công ty cung ứng nguồn lao động hàng đầu tại Việt Nam.
													</p>

													<p style="text-align: justify;">
														<strong>3.</strong> Luôn lấy chữ tín, sự tận tâm với công việc nền tảng cho mọi hoạt động kinh doanh của JVNET.
													</p>

													<p style="text-align: justify;">
														<strong>4.</strong> Lấy chất lượng làm cam kết với đối tác và người lao động.
													</p>

													<p style="text-align: justify;">
														<strong>5.</strong> Luôn phấn đấu giữ vững và phát triển uy tín thương hiệu của JVNET.
													</p>
												</div>

												<div class="col-sm-4 mrb20 text-center">
													<img alt="" src="<?php echo bloginfo('template_directory');?>/images/hinh3.jpg">
												</div>
											</div>
										</div>
									</div>


									<div class="content mrb40" id="part-5">
									    <h2>Lịch Sử Hình Thành</h2>
									    <div class="desc clearfix">
										    <div class="row">
												<div class="col-sm-5 mrb20">
													<img alt="" src="<?php echo bloginfo('template_directory');?>/images/hinh4.jpg">
												</div>

												<div class="col-sm-7 mrb20">
													<p style="text-align: justify;">
														<strong>Tháng 9/2005,</strong> công ty cổ phần Thương mại Phát triển Kỹ thuật và nhân lực Quốc tế được thành lập.
													</p>

													<p style="text-align: justify;">
														Trải qua 10 năm phát triển và trưởng thành, JVNET đã và đang từng bước khẳng định được vị thế của mình, tự hào là một trong số các doanh nghiệp uy tín hàng đầu Việt Nam trong lĩnh vực xuất khẩu lao động.
													</p>

													<p style="text-align: justify;">
														Trải qua từng thời kỳ, với bao thăng trầm, vất vả trên con đường xây dựng thương hiệu JVNET, sự nỗ lực của tập thể cán bộ nhân viên cùng với sự tin tưởng của các đối tác đã góp phần thay đổi lớn về tầm vóc, vị thế cũng như uy tín của JVNET trong lĩnh vực cung ứng nguồn lao động cho thị trường Nhật Bản hiện nay.
													</p>
												</div>
											</div>
											<table style="text-align: justify;">
											    <tbody>
											        <tr>
											            <td>
											            	<em class="fa fa-quote-left"></em>
											            </td>

											            <td>
											            Trong tương lai, chúng tôi sẽ không ngừng phấn đấu để xứng đáng là địa chỉ tin cậy cho tất cả mọi người có ước mơ làm việc và học tập tại Nhật Bản.
											            </td>
											        </tr>
											    </tbody>
											</table>
											<div style="text-align: justify;"><br>
											</div>
										</div>
									</div>
							</div>
							    
							    

							</div>
						</div>
					</div>
								    
				</div><!--hết content-->

				<div id="ctl00_divRight" class="col-md-3 col-right cmszone sidebar">
					<?php get_template_part('sidebar'); ?>
				</div>
			</div>
		</div>
	</div>
</div>





<?php get_footer(); ?>