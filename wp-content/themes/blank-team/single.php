<?php get_header(); ?>
<div class="content single-vieclam" id="single-post">
	<div class="container">
		<div class="section-breadcrum">
			<ol class="breadcrumb">
	            <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
		            <a href="<?php echo site_url(); ?>" class="itemcrumb" itemprop="url">
		            	<span itemprop="title">Trang chủ</span>
		            </a>
	            </li>
	            <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
		            <a href="<?php echo site_url(); ?>" class="itemcrumb active" itemprop="url">
		            	<span itemprop="title">Thông tin việc làm - đào tạo</span>
		            </a>
	            </li>
	            <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
	            	<span itemprop="title"><?php the_title(); ?></span>
	            </li>
			</ol>
		</div>


		<div class="section-content">
			<div class="row">
				<div id="ctl00_divCenter" class="col-md-9 col-sm-9 col-xs-12 col-main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<div class="post-container">
							
							<h2><?php the_title(); ?></h2>
							
							<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>
							<hr/>
							<div class="entry">
								
								<?php the_content(); ?>

								<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
								
								<?php the_tags( 'Tags: ', ', ', ''); ?>

							</div>
							
						</div>

					<?php endwhile; endif; ?>
					<hr>
					<div class="news-other mrb20 clearfix">
					    <h4>Các tin khác</h4>
					    
					</div>
				</div>

				<div id="ctl00_divRight" class="col-md-3 col-sm-3 col-xs-12 col-right">
					<?php get_template_part('sidebar'); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>