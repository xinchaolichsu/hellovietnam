<div class="meta">
	<?php the_time('F jS, Y') ?> - <em>by</em> <span style="color: #2375BC"><?php the_author() ?></span>
</div>