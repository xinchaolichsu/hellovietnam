<?php
$vendor_autoload_file = __DIR__ . '/vendor/autoload.php';
if (file_exists($vendor_autoload_file)) {
    require $vendor_autoload_file;
}
use App\SettingHellovietnam;
use App\Widget\WidgetSupportPhone;

define('THEME_NAME', 'hellovietnam');

define('TEMPLATEPATH', get_template_directory());
define('CSSPATH', TEMPLATEPATH . '/css');

new SettingHellovietnam();
require_once(TEMPLATEPATH . '/SettingTheme/custom-post-type.php');

new WidgetSupportPhone(); 
?>