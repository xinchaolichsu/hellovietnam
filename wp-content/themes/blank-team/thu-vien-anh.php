<?php
/*
Template Name:thu-vien-anh
*/
get_header();
?>

<style>
    /* jssor slider arrow navigator skin 02 css */
    /*
    .jssora02l                  (normal)
    .jssora02r                  (normal)
    .jssora02l:hover            (normal mouseover)
    .jssora02r:hover            (normal mouseover)
    .jssora02l.jssora02ldn      (mousedown)
    .jssora02r.jssora02rdn      (mousedown)
    .jssora02l.jssora02lds      (disabled)
    .jssora02r.jssora02rds      (disabled)
    */
    .jssora02l, .jssora02r {
        display: block;
        position: absolute;
        /* size of arrow element */
        width: 55px;
        height: 55px;
        cursor: pointer;
        background: url('img/a02.png') no-repeat;
        overflow: hidden;
    }
    .jssora02l { background-position: -3px -33px; }
    .jssora02r { background-position: -63px -33px; }
    .jssora02l:hover { background-position: -123px -33px; }
    .jssora02r:hover { background-position: -183px -33px; }
    .jssora02l.jssora02ldn { background-position: -3px -33px; }
    .jssora02r.jssora02rdn { background-position: -63px -33px; }
    .jssora02l.jssora02lds { background-position: -3px -33px; opacity: .3; pointer-events: none; }
    .jssora02r.jssora02rds { background-position: -63px -33px; opacity: .3; pointer-events: none; }
    /* jssor slider thumbnail navigator skin 03 css *//*.jssort03 .p            (normal).jssort03 .p:hover      (normal mouseover).jssort03 .pav          (active).jssort03 .pdn          (mousedown)*/.jssort03 .p {    position: absolute;    top: 0;    left: 0;    width: 62px;    height: 32px;}.jssort03 .t {    position: absolute;    top: 0;    left: 0;    width: 100%;    height: 100%;    border: none;}.jssort03 .w, .jssort03 .pav:hover .w {    position: absolute;    width: 60px;    height: 30px;    border: white 1px dashed;    box-sizing: content-box;}.jssort03 .pdn .w, .jssort03 .pav .w {    border-style: solid;}.jssort03 .c {    position: absolute;    top: 0;    left: 0;    width: 62px;    height: 32px;    background-color: #000;    filter: alpha(opacity=45);    opacity: .45;    transition: opacity .6s;    -moz-transition: opacity .6s;    -webkit-transition: opacity .6s;    -o-transition: opacity .6s;}.jssort03 .p:hover .c, .jssort03 .pav .c {    filter: alpha(opacity=0);    opacity: 0;}.jssort03 .p:hover .c {    transition: none;    -moz-transition: none;    -webkit-transition: none;    -o-transition: none;}* html .jssort03 .w {    width /**/: 62px;    height /**/: 32px;}
	</style>

<div class="content-thuvienanh">
	<div class="container">
		<div id="ctl00_divAlt1" class="altcontent1 container cmszone">
            <div class="row Module Module-141">
                <ol class="breadcrumb">
		            <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
			            <a href="#" class="itemcrumb" itemprop="url">
			            	<span itemprop="title">Trang chủ</span>
			            </a>
		            </li>
		        
		            <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
			            <a href="#" class="itemcrumb" itemprop="url">
			            	<span itemprop="title">Tin tức - sự kiện</span>
			            </a>
		            </li>
		        
		            <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
			            <a href="#" class="itemcrumb active" itemprop="url">
			            	<span itemprop="title">Hình ảnh - Video clips</span>
			            </a>
		            </li>    
				</ol>
			</div>
		            
		</div>
		

		<div class="row">
            <div class="col-md-9">
            	<div class="ModuleContent">
            		<h1 class="title-page">Hình ảnh - Video clips</h1>

            		<div class="col-md-4 col-sm-4 mrb30">
            			<article class="album-item">
							<h2>Thiện nguyện Bạch Ngọc</h2>

							<a class="fancybox-thumbs popup-galary" href="#"><img class="images-thuvien img-responsive" data-toggle="modal" data-target="#myModal" src="<?php echo bloginfo('template_directory');?>/images/01.jpg" alt="Thư Viện Ảnh">
							
								<div class="container">
								  <!-- Modal -->
								  <div class="modal fade" id="myModal" role="dialog">
								    <div class="modal-dialog modal-lg">
								      <div class="modal-content">
								        <div class="modal-header">
								          <button type="button" class="close" data-dismiss="modal">&times;</button>
								          <h4 class="modal-title">Ảnh Khai Giảng</h4>
								        </div>
								        <div class="modal-body">
								          	    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:600px;height:300px;overflow:hidden;visibility:hidden;">
											        <!-- Loading Screen -->
											        <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
											            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
											            <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
											        </div>
											        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:600px;height:300px;overflow:hidden;">
											            <div>
											                <img data-u="image" src="<?php echo bloginfo('template_directory');?>/images/003.jpg" />
											                <img data-u="thumb" src="<?php echo bloginfo('template_directory');?>/images/thumb-003.jpg" />
											            </div>
											            <div>
											                <img data-u="image" src="<?php echo bloginfo('template_directory');?>/images/003.jpg" />
											                <img data-u="thumb" src="<?php echo bloginfo('template_directory');?>/images/thumb-003.jpg" />
											            </div>
											            <div>
											                <img data-u="image" src="<?php echo bloginfo('template_directory');?>/images/003.jpg" />
											                <img data-u="thumb" src="<?php echo bloginfo('template_directory');?>/images/thumb-003.jpg" />
											            </div>
											            <div>
											                <img data-u="image" src="<?php echo bloginfo('template_directory');?>/images/003.jpg" />
											                <img data-u="thumb" src="<?php echo bloginfo('template_directory');?>/images/thumb-003.jpg" />
											            </div>
											           
											        </div>
											        <!-- Thumbnail Navigator -->
											        <div data-u="thumbnavigator" class="jssort03" style="position:absolute;left:0px;bottom:0px;width:600px;height:60px;" data-autocenter="1">
											            <div style="position:absolute;top:0;left:0;width:100%;height:100%;background-color:#000;filter:alpha(opacity=30.0);opacity:0.3;"></div>
											            <!-- Thumbnail Item Skin Begin -->
											            <div data-u="slides" style="cursor: default;">
											                <div data-u="prototype" class="p">
											                    <div class="w">
											                        <div data-u="thumbnailtemplate" class="t"></div>
											                    </div>
											                    <div class="c"></div>
											                </div>
											            </div>
											            <!-- Thumbnail Item Skin End -->
											        </div>
											        <!-- Arrow Navigator -->
											        <span data-u="arrowleft" class="jssora02l" style="top:0px;left:8px;width:55px;height:55px;" data-autocenter="2"></span>
											        <span data-u="arrowright" class="jssora02r" style="top:0px;right:8px;width:55px;height:55px;" data-autocenter="2"></span>
											    </div>
											    <!-- #endregion Jssor Slider End -->
								        </div>
								        <div class="modal-footer">
								          <button type="button" class="btn btn-default" data-dismiss="modal">	Close
								          </button>
								        </div>
								      </div>
								    </div>
								  </div>
								</div>
							

								<div class="count-anh">
									<span>
						              <a class="a-thuvienanh" href="#">(23 hình ảnh)</a>
						            </span>
					            </div>

							</a>
			       		</article>
            		</div>

            		<div class="col-md-4 col-sm-4 mrb30">
            			<article class="album-item">
							<h2>Thiện nguyện Bạch Ngọc</h2>

							<a class="fancybox-thumbs popup-galary" rel="fancybox-thumb13" href="/Data/Sites/1/ImageGallery/13/01.jpg" title="Thiện nguyện Bạch Ngọc">
								<div class="center-block album-img">
									<a href="#">
										<img src="<?php echo bloginfo('template_directory');?>/images/01.jpg" alt="Thiện nguyện Bạch Ngọc">
									</a>
								</div>

								<div class="count">
									<i class="fa fa-picture-o"></i> 
									<span>
						              <a href="#">(23 hình ảnh)</a>
						            </span>
					            </div>
				            </a>
			            </article>
            		</div>

            		<div class="col-md-4 col-sm-4 mrb30">
            			<article class="album-item">
							<h2>Thiện nguyện Bạch Ngọc</h2>

							<a class="fancybox-thumbs popup-galary" rel="fancybox-thumb13" href="/Data/Sites/1/ImageGallery/13/01.jpg" title="Thiện nguyện Bạch Ngọc">
								<div class="center-block album-img">
									<a href="#">
										<img src="<?php echo bloginfo('template_directory');?>/images/01.jpg" alt="Thiện nguyện Bạch Ngọc">
									</a>
								</div>

								<div class="count">
									<i class="fa fa-picture-o"></i> 
									<span>
						              <a href="#">(23 hình ảnh)</a>
						            </span>
					            </div>
				            </a>
			            </article>
            		</div>

					<script src="<?php echo bloginfo('template_directory');?>/js/jssor.slider-23.1.1.mini.js" type="text/javascript"></script>
					<script src="<?php echo bloginfo('template_directory');?>/js/jquery-1.11.3.min.js" type="text/javascript"></script>
					<script type="text/javascript">
				        jQuery(document).ready(function ($) {

				            var jssor_1_options = {
				              $AutoPlay: 1,
				              $ArrowNavigatorOptions: {
				                $Class: $JssorArrowNavigator$
				              },
				              $ThumbnailNavigatorOptions: {
				                $Class: $JssorThumbnailNavigator$,
				                $Cols: 9,
				                $SpacingX: 3,
				                $SpacingY: 3,
				                $Align: 260
				              }
				            };

				            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

				            /*responsive code begin*/
				            /*remove responsive code if you don't want the slider scales while window resizing*/
				            function ScaleSlider() {
				                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
				                if (refSize) {
				                    refSize = Math.min(refSize, 600);
				                    jssor_1_slider.$ScaleWidth(refSize);
				                }
				                else {
				                    window.setTimeout(ScaleSlider, 30);
				                }
				            }
				            ScaleSlider();
				            jQuery(window).bind("load", ScaleSlider);
				            jQuery(window).bind("resize", ScaleSlider);
				            jQuery(window).bind("orientationchange", ScaleSlider);
				            /*responsive code end*/
				        });
				    </script>
            	</div>
            </div>

            <div class="col-md-3 col-sm-3">
            	<?php get_template_part('sidebar'); ?>
            </div>
		</div>
	</div><!--end container-thu-vien-anh-->
</div><!--end content-thuvienanh-->



<?php get_footer();?>