<?php
/*
Template Name:lienhe
*/
get_header();
?>

<div class="container contact">
	<div class="container">
			<div class="row Module Module-141">
				<ol class="breadcrumb" style="background: none;">

		            <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="#" class="itemcrumb" itemprop="url"><span itemprop="title">Trang chủ</span></a></li>
		        
		            <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="#" class="itemcrumb active" itemprop="url"><span itemprop="title">Liên Hệ</span></a></li>
				        
				    
				</ol>
			</div>
		</div>
	<div class="row">
		<div id="ctl00_divCenter" class="middle-fullwidth col-sm-12">
			<div class="clearfix contact-wrap">
				<div class="col-sm-12 Module Module-54">
					<div class="ModuleContent">
						<h1 class="title-page">
						Liên hệ
						</h1>
					</div>
				</div>

				<div class="col-md-4 col-sm-6 mrb20 Module Module-53">
					<div class="ModuleContent">

					<h2>1. Liên hệ về Chương trình Thực tập sinh kỹ năng Nhật Bản (Xuất khẩu lao động)</h2>

					<p>Phụ trách: Lê Văn Hồng (Phòng Nghiệp vụ)</p>

					<p>Email: v-hong@jvnet.com.vn</p>

					<p>SĐT: 04 3748 1982</p>
					<br>

					<h2>2. Liên hệ về Chương trình du học Nhật Bản</h2>

					<p>Phụ trách: Trần Văn Lợi</p>

					<p>Email: v-loi@jvnet.com.vn</p>

					<p>SĐT：04 3755 8480</p>
					<br>

					<h2>3. Liên hệ về Chương trình Kỹ sư làm việc tại Nhật Bản</h2>
					<p>Phụ trách: Trần Phương Tâm</p>

					<p>Email: tam.jvnet@gmail.com</p>

					<p>SĐT: 04 3748 1982</p>
					<br>

					</div>
				</div>


				<div class="col-md-4 col-sm-6 mrb20">
					<div class="clearfix mrb25 Module Module-146">
						<div class="ModuleContent">
							<h2>Liên hệ qua email:</h2>
							Xin vui lòng để lại thông tin, tất cả các thông tin được đánh dấu * là bắt buộc.
						</div>
					</div>

					<div class="contact-form Module Module-93">
						<div class="ModuleContent">
							<div class="wrap-contact">
							    <div id="ctl00_mainContent_ctl03_upContact">
									
							            <div id="ctl00_mainContent_ctl03_pnlSend" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'ctl00_mainContent_ctl03_btnSend')">
										
							                <div class="form-group ct-name">
							                    <label for="ctl00_mainContent_ctl03_txtName" class="label">
							                    	Họ tên<span>*</span>
							                    </label>

							                    <input name="ctl00$mainContent$ctl03$txtName" type="text" maxlength="100" id="ctl00_mainContent_ctl03_txtName" class="form-control" placeholder="Họ tên">

							                    <span id="ctl00_mainContent_ctl03_reqName" title="Vui lòng nhập họ tên." class="fa fa-exclamation-triangle" style="display:none;">
							                    	
							                    </span>
							                </div>
							                <div class="form-group ct-address">
							                    <label for="ctl00_mainContent_ctl03_txtAddress" class="label">
							                    	Địa chỉ
							                    </label>

							                    <input name="ctl00$mainContent$ctl03$txtAddress" type="text" maxlength="255" id="ctl00_mainContent_ctl03_txtAddress" class="form-control" placeholder="Địa chỉ">
							                    
							                </div>

							                <div class="form-group ct-email">
							                    <label for="ctl00_mainContent_ctl03_txtEmail" class="label">
							                    	Email<span>*</span>
							                    </label>
							                    <input name="ctl00$mainContent$ctl03$txtEmail" type="text" maxlength="255" id="ctl00_mainContent_ctl03_txtEmail" class="form-control" placeholder="Email">
							                    <span id="ctl00_mainContent_ctl03_reqEmail" title="Vui lòng nhập email." class="fa fa-exclamation-triangle" style="display:none;"></span>
							                    <span id="ctl00_mainContent_ctl03_regexEmail" title="Email không hợp lệ." class="fa fa-exclamation-triangle" style="display:none;"></span>
							                </div>
							                
							                <div class="form-group ct-phone">
							                    <label for="ctl00_mainContent_ctl03_txtPhone" class="label">
							                    	Điện thoại
							                    </label>
							                    <input name="ctl00$mainContent$ctl03$txtPhone" type="text" maxlength="255" id="ctl00_mainContent_ctl03_txtPhone" class="form-control" placeholder="Điện thoại">
							                    
							                </div>
							                
							                <div class="form-group ct-subject">
							                    <label for="ctl00_mainContent_ctl03_txtSubject" class="label">
							                    	Tiêu đề
							                    </label>
							                    <input name="ctl00$mainContent$ctl03$txtSubject" type="text" maxlength="255" id="ctl00_mainContent_ctl03_txtSubject" class="form-control" placeholder="Tiêu đề">
							                </div>
							                <div class="form-group ct-message">
							                    <label for="ctl00_mainContent_ctl03_txtMessage" class="label">Nội dung<span>*</span></label>
							                    <textarea name="ctl00$mainContent$ctl03$txtMessage" rows="2" cols="20" id="ctl00_mainContent_ctl03_txtMessage" class="form-control" placeholder="Nội dung"></textarea>
							                    <span id="ctl00_mainContent_ctl03_reqMessage" title="Vui lòng nhập nội dung liên hệ." class="fa fa-exclamation-triangle" style="display:none;"></span>
							                </div>
							                
							                <div class="clear"></div>
							                <div class="form-group ct-button">
							                    <div class="frm-btnwrap">
							                        <div class="frm-btn">
							                            <input type="submit" name="ctl00$mainContent$ctl03$btnSend" value="Gửi" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$mainContent$ctl03$btnSend&quot;, &quot;&quot;, true, &quot;Contact93&quot;, &quot;&quot;, false, false))" id="ctl00_mainContent_ctl03_btnSend" class="ct-button btn btn-default">
							                        </div>
							                    </div>
							                </div>
							            
									</div>
							        <div class="clear"></div>
							            
							        
								</div>
							</div>
						</div>
					</div>
				</div>
				

				<div class="col-md-4 col-sm-6 text-center mrb20 Module Module-147">
					<div class="ModuleContent">
						<img alt="" src="<?php echo bloginfo('template_directory');?>/images/contact.jpg">
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 mrb20 Module Module-148">
					<div class="ModuleContent">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.659501398719!2d105.78810631450513!3d21.046305985988894!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab31d629837f%3A0x3e0c3a0cf76a9b72!2zQ8O0bmcgdHkgWHXhuqV0IGto4bqpdSBsYW8gxJHhu5luZyBOaOG6rXQgQuG6o24gLSBKVk5FVA!5e0!3m2!1svi!2s!4v1476932449796" width="100%" height="350" frameborder="0" style="border: 0px;">
						
					</iframe>
					</div>
				</div>


				<div class="col-md-6 col-sm-12 mrb20 Module Module-149">
					<div class="ModuleContent">
						<h2>Công ty Cổ phần Thương mại Phát triển Kỹ thuật và Nhân lực Quốc tế (JVNET).</h2>

						<p>Địa chỉ : 30 Trần Cung - Từ Liêm - Hà Nội</p>

						<p>VPGD: Tầng 7, tòa nhà Pvoil, số 148 Hoàng Quốc Việt, Cầu Giấy, Hà Nội</p>

						<p>TEL: +84-43-7556251  </p>

						<p>FAX: +84-43-7556254 </p>

						<p>EMAIL: info@jvnet.com.vn</p>
						<br>

						<h2>Văn phòng đại diện tại tp Hồ Chí Minh</h2>

						<p>Địa chỉ: Lầu 4, toà nhà HT Building, 132 -134 D2, phường 25, Bình Thạnh, Thành Phố Hồ Chí Minh</p>

						<p>Điện thoại:08-62948878 / 08-62948869</p>

						<p>Fax: 08-62948895</p>

						<br>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>






<?php get_footer(); ?>