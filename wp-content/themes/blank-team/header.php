<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>

	<title>
		   <?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' Archive - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_home()) {
		         bloginfo('name'); echo ' - '; bloginfo('description'); }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>


	</title>

	
	<link rel="shortcut icon" href="/favicon.ico">
	
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>

	<?php if ( is_singular() ) wp_enqueue_script('comment-reply'); ?>

	
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="<?php echo bloginfo('template_directory');?>/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
	<link rel="stylesheet" href="<?php echo bloginfo('template_directory').'/dist/styles.css'; ?>"/>
		
	<?php wp_head(); ?>

		
</head>

<body <?php body_class(); ?>>
	<div id="wrapper">
		<header class="container">
			<div class="row header-logo">
				<div class="col-md-4 col-sm-4 col-xs-12 header-logo">
					<a href="<?php echo get_home_url(); ?>">
						<?php the_custom_logo();?>
					</a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 form-search">
					<?php get_search_form(true); ?>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 header-right">
						<section class="header-right text-right">
                            <?php 
                            if(is_active_sidebar('header_sidebar')) {
                            	dynamic_sidebar('header_sidebar');
                            } 
                            ?>
                            <div class="dang-ky pull-right"><div class="ModuleContent"><a href="http://soccon.com.vn"><em class="fa fa-lock"></em> Giỏ hàng</a></div></div>
                    </section>
				</div>
			</div>
			
			<div class="menu">
				<nav class="navbar navbar-default menu-home" role="navigation">
					<div class="container-fluid">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
							<?php
								wp_nav_menu([
								    'container' => 'div',
								    'container_class' => 'collapse navbar-collapse',
								    'theme_location' => 'main_nav',
								    'menu_class' => 'nav navbar-nav',
								]);
							?>
					</div>
				</nav>
			</div>
		</header>